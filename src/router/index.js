import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import HelloJang from '@/components/HelloJang'
import jang1 from '@/components/jang/jang1'
import jang2 from '@/components/jang/jang2'
import hi1 from '@/components/hi/hi1'
import hi2 from '@/components/hi/hi2'
import pgwithparams from '@/components/pagewithparams'
import spwiththreeview from '@/components/spwiththreeview'
import paramswithurl from '@/components/paramswithurl'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path:'*',
      redirect:'/'
    },
    {
      path: '/',
      name: 'HelloWorld',
      component: jang1
    },

    // 子路由
    {
      path:'/jang',
      children:[
        // jang默认页面router-view渲染HelloJang组件
        {path:'/'},
        {path:'jang1',component:jang1},
        {path:'jang2',component:jang2},
      ],
      component:HelloJang
    },
    // 带有参数的路由
    {
      path:'/pgparams',
      name:'pgparams',
      component:pgwithparams
    },
    {
      path:'/spwiththreeview',
      name:'spwiththreeview',
      components:{
        default:spwiththreeview,
        left:hi1,
        right:hi2
      },
      alias:'/spv'
    },
    {
      path:'/paramswithurl/:username/:role',
      name:'paramswithurl',
      component:paramswithurl
    },
    {
      path:'/gohome',
      name:'gohome',
      redirect:'/paramswithurl/uu2/reuu'
    }
  ]
})

